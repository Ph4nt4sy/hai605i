
# TP1 Prise en main d'une librairie de traitement d'images

## 1) Seuillage d'une image au format pgm

####    1. Les fichiers permettent de : 
 
<ol>
  <li>  Flouter une image qui est uniquement en niveau de gris: blur.cpp</li>
  <li>  Seuiller une image qui est en couleur: test_couleur.cpp</li>
  <li>  Teste si c'est une image bien formée: image_ppm.cpp</li>
  <li>  Seuiller une image qui est en niveau de gris: test_gris.cpp</li>
  <li>  Le fichier images_ppm.h avec toutes les fonctions utilisées dans les fichiers présentés ci-dessus.</li>
</ol>

####    2. Ce sont différents formats de fichiers graphiques:
<ul>
  <li> image ppm : pixmap file format (images en couleur)</li>
  <li> image pgm : graymap file format (images en niveau de gris)</li>
</ul>

####    3. Compilation en c++
Pour compiler en c++ on doit faire:
> g++ test_grey.cpp -o test_grey

####    4. Test des valeurs de seuil.

On avait cette image de base:<br>
<img src="TP1/img/01.png" alt="01.png" width="200"/>

On obtient celle-ci avec le seuil de 50:<br>
<img src="TP1/img/test1.png" alt="test1.png" width="200"/>

On peut aussi avoir celle-la avec le seuil de 200:<br>
<img src="TP1/img/test2.png" alt="test2.png" width="200"/>

Finalement, on peut avoir cette image avec le seuil de 125:<br>
<img src="TP1/img/test3.png" alt="test3.png" width="200"/>

## 2) Seuillage d’une image pgm avec plusieurs niveaux S1, S2, S3

#### 1. En 3 parties

Le truc de ce programme ([test_grey_v2.cpp](TP1/test_grey_v2.cpp)) c'est de jouer sur les seuils. Ici on a une expression ternaire:
> ImgIn\[i\*nW+j] < S1? ImgOut\[i\*nW+j]=0 : ImgIn\[i\*nW+j] < S2? ImgOut\[i\*nW+j]=125 : ImgOut\[i\*nW+j]=255;


Soit on est inférieur au premier seuil. On va donc créer un pixel blanc.<br>
Sinon, si on est inférieur au deuxième seuil, on va créer un pixel gris.<br>
Sinon, on créer un pixel noir.

Sinon, le reste du programme est le même que celui du [test_grey.cpp](TP1/test_grey.cpp)

Voici une première image obtenue avec les seuils 50 et 200:<br>
<img src="TP1/img/test4.png" alt="test4.png" width="200"/>

Une deuxième image obtenue avec les seuils 100 et 150:<br>
<img src="TP1/img/test5.png" alt="test5.png" width="200"/>



#### 2. En 4 parties

Le programme [test_grey_v3.cpp](TP1/test_grey_v3.cpp) revient au même que la version v2. Seulement, au lieu de jouer avec un seuil, 
on joue avec deux seuils. Voici la partie de code qui diffère des deux fichiers antérieurs:

> ImgIn\[i\*nW+j] < S1? ImgOut\[i*\nW+j]=0 : ImgIn\[i\*nW+j] < S2? ImgOut\[i\*nW+j]=65 : ImgIn\[i\*nW+j] < S3? ImgOut\[i\*nW+j]=190 :ImgOut\[i\*nW+j]=255;

Cette fois-ci, pas besoin d'expliquer la ligne de code, on a juste un seuil de plus et des valeurs intermédiaires.

Cette image est obtenue avec les seuils 50 125 200:<br>
<img src="TP1/img/test6.png" alt="test6.png" width="200">

Celle-la est obtenue avec les seuils 75 150 225:<br>
<img src="TP1/img/test7.png" alt="test7.png" width="200">


## 3) Histogramme d'une image pgm


#### 1. Création du fichier

Le programme [histogramme.cpp](TP1/histogramme.cpp) permet de sauvegarder dans un tableau de 256 cases. L'indice de la case est la valeur du niveau de gris d'un pixel, sa valeur est elle le nombre d'occurence de ce niveau de gris dans l'image.

#### 2. Compilation

Lorsque l'on compile le programme grâce à cette commande:
>./histogramme 01.pgm

On obtient alors un fichier nommé [histo.dat](TP1/histo.dat) avec en première colonne l'indice du tableau, après une indentation
on a le nombre d'occurence de la nuance de gris.

#### GNUPLOT

Après la compilation, grâce au fichier histo.dat, on peut avoir un graphe qui fait un histogramme comme celui ci-dessous:<br>
<img src="TP1/img/histogramme.png" alt="histogramme.png" width="200"/>

On l'a grâce à ces commandes executées dans l'ordre:

>gnuplot <br>
>plot "histo.dat" with lines


## 4) Profil d'une ligne ou d'une colonne d'une image pgm

Le programme [profil.cpp](TP1/profil.cpp) permet de faire un graphe à partir du fichier [profil.dat](TP1/profil.dat) qui prend en abscisse le pixel d'une ligne ou d'une colonne et 
en ordonnée nous représente le niveau de gris du pixel grâce a ces commandes:
>gnuplot<br>
>plot "fich.dat" with lines


On obtient alors ce graphe:<br>
<img src="TP1/img/profil.png" alt="profil.png" width="200"/>

## 5) Seuillage d’une image couleur (ppm)

Le programme [seuillage_couleur.cpp](TP1/seuillage_couleur.cpp) permet de faire un seuillage avec trois seuils différents qui
servent pour la nuance de Rouge, de Vert et de Bleu.
Sinon, à part ces lignes de codes, rien n'a changé par rapport au programme [test_couleur.cpp](TP1/test_couleur.cpp):
>if (nR < SR) ImgOut[i]=0; else ImgOut[i]=255;<br>
>if (nG < SG) ImgOut[i+1]=0; else ImgOut[i+1]=255; <br>
>if (nB < SB) ImgOut[i+2]=0; else ImgOut[i+2]=255;<br>

On voit ici nR, nG et nB qui sont les nuances de Rouge, Vert et Bleu du fichier.
On les compare aux trois seuils explicités ci-dessus. Puis, comme pour l'histogramme, si on est en dessous du seuil, le pixel prend
la nuance de 0 ou de 255 si on est au dessus du seuil.

On a cette image de base:<br>
<img src="TP1/img/pepper.png" alt="pepper.png" width="200"/>

Puis, avec un seuil de 50, 150 et 200 on se retrouve avec cette image:<br>
<img src="TP1/img/pepper1.png" alt="pepper1.png" width="200"/>


Avec un seuil de 125 125 125 on obtient cette image:<br>
<img src="TP1/img/pepper2.png" alt="pepper2.png" width="200"/>

On obtient le même résultat qu'en utilisant le programme [test_couleur.cpp](TP1/test_couleur.cpp) avec un seuil de 125:<br>
<img src="TP1/img/pepper3.png" alt="pepper3.png" width="200"/>

Grâce à ce résultat, on sait que notre programme seuillage_couleur.cpp fonctionne comme on le voulait.

## 6) Histogrammes des 3 composantes d'une image couleur (ppm)

Pour ce [programme](TP1/histocoul.cpp) j'ai utilisé la même méthode que pour [histogramme.cpp](TP1/histogramme.cpp), seulement, au lieu d'avoir un tableau, j'en ai trois (une pour chaque nuance, soit respectivement Rouge, Vert et Bleu) et ainsi on les écrit dans
le fichier [histocoul.dat](TP1/histocoul.dat) avec en premier élément l'indice du tableau, en deuxième élément le nombre 
d'occurence de nuance de Rouge, le troisième pour l'occurence de nuance de Vert et enfin pour le Bleu.

On obtient alors ce graphe:<br>
<img src="TP1/img/histocoul.png" alt="histocoul.png" width="200"/>

Grâce à ces trois commandes:
>plot "histocoul.dat" using 1:2 title 'Red' lt rgb "red" with lines,<br>
>"histocoul.dat" using 1:3 title 'Green' lt rgb "green" with lines,<br>
>"histocoul.dat" using 1:4 title 'Blue' lt rgb "blue" with lines


## En conclusion

J'ai trouvé que ce TP était très bien pour commencer, et la seule difficulté rencontré à été la lecture des énoncés, car j'ai mis plus de 3 heures à comprendre ce qu'il fallait faire pour le seuillage d'une image en couleur. Une fois que j'avais compris l'énoncé
ça a été très rapide d'écrire le code (qui était un copié collé de test_couleur.cpp).

