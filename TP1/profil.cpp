#include <stdio.h>
#include <fstream>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250];
  int nH, nW, nTaille,lc,ilc;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ligne/col indice:ligne/col \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%d",&lc);
   sscanf (argv[3],"%d",&ilc);

   OCTET *ImgIn;

   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   allocation_tableau(ImgIn, OCTET, nH*nW);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

    if (lc==0)
    {
        int tableau [nH];
        for (int i=0;i<nW;i++)
        {
            tableau[i]=ImgIn[i*nW+ilc];
        }

        std::ofstream fichier {"profil.dat"};

        for (int i=0;i< nW;i++)
        {
            fichier <<i<<"\t"<<tableau[i]<<"\n"; 
        }

   fichier.close();
    }
    else
    {
        int tableau [nW];
        for (int i=0;i<nH;i++)
        {
            tableau[i]=ImgIn[ilc*nW+i];
        }


        std::ofstream fichier {"profil.dat"};

                for (int i=0;i< nH;i++)
                {
                    fichier <<i<<"\t"<<tableau[i]<<"\n"; 
                }

        fichier.close();

    }
    
    
    


   free(ImgIn); 

   return 1;

}