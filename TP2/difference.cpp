#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250],cNomImg2Lue[250] , cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn1.pgm ImageIn2.pgm ImgOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImg2Lue) ;
   sscanf (argv[3],"%s",cNomImgEcrite);

   OCTET *ImgIn,*ImgIn2, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   lire_image_pgm(cNomImg2Lue, ImgIn2, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

 for (int i=1; i < nH-1; i++){
   for (int j=1; j < nW-1; j++)
     {
        if (ImgIn[i*nW+j]==ImgIn2[i*nW+j] && ImgIn[i*nW+j]==255)
       {
           ImgOut[i*nW+j]=255; 
       }
       else
       {
           ImgOut[i*nW+j]=0; 
       } 
     }
 }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut); free(ImgIn2);

   return 1;
}
