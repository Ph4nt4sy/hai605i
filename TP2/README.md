
# TP2 Opérations morphologiques sur des images

## 1) Seuillage d’une image et érosion de l’image binaire

#### 1. Réduction de la taille de l'image vers un format de 256*256 pixel

Pour ce TP, j'ai prit cette image initiale:<br>
<img src="TP2/img/slenderman0.png" alt="slenderman0.png" width="200"/>


Qui a donné ça une fois convertit au bon format, mais aussi en noir et blanc.<br>
<img src="TP2/img/slenderman1.png" alt="slenderman1.png" width="200"/>


#### 2. Tests de seuillage

Pour le seuillage, nous allons utiliser l'ancien programme [test_grey.cpp](TP1/test_grey.cpp):

>./test_grey slenderman0.pgm slenderman1.pgm 60
<img src="TP2/img/slenderman1.png" alt="slenderman1.png" width="200"/>

>./test_grey slenderman0.pgm slenderman2.pgm 200
<img src="TP2/img/slenderman2.png" alt="slenderman2.png" width="200"/>

>./test_grey slenderman0.pgm slenderman3.pgm 125
<img src="TP2/img/slenderman3.png" alt="slenderman3.png" width="200"/>

<br>Pour la suite du TP, nous prendrons cette image:<br>

>./test_grey slenderman0.pgm slenderman4.pgm 100
<img src="TP2/img/slenderman4.png" alt="slenderman4.png" width="200"/>


#### 3. Création du fichier erosion.cpp

Pour le programme [erosion.cpp](TP2/erosion.cpp), nous devons faire en sorte que les points objets (donc blancs) qui sont isolés
ou qui sont les bords des objets soient passé en points de fond (en noir).

On a donc ce résultat:
>./erosion slenderman4.pgm erosion1.pgm
<img src="TP2/img/erosion1.png" alt="erosion1.png" width="200"/>


## 2) Seuillage d’une image et dilatation de l’image binaire

#### 1. Création du fichier dilatation.cpp

Pour le programme [dilatation.cpp](TP2/dilatation.cpp), le principe est l'inversion de l'érosion.<br>
Ici, nous devons supprimés les points de fond isolés, mais aussi le bords du fond afin de les faire passer en points objets.

Voici le résultat sur l'image choisie pour le TP:
>./dilatation slenderman4.pgm dilatation1.pgm
<img src="TP2/img/dilatation1.png" alt="dilatation.png" width="200"/>


## 3) Fermeture et ouverture d’une image de l’image binaire


#### 1. Création du fichier fermeture.cpp

Le programme [fermeture.cpp](TP2/fermeture.cpp) permet de combiner une dilatation et une érosion. Cela a pour but de fermer les trous présents dans l'objet(en blanc) grâce à la dilatation, puis ensuite de recadrer l'objet grâce à l'érosion, afin qu'il regagne une taille presque identique à l'image d'origine.


#### 2. Création du fichier ouverture.cpp

Le programme [ouverture.cpp](TP2/ouverture.cpp) permet lui de combiner une érosion et une dilatation. Cela permettra déjà de perdre les points objets (blancs) perdus un peu partout sur l'image. Ensuite on va réagrandir l'objet, parce qu'elle a rétrécie à cause de l'érosion. 

#### 3. Enchaîner une fermeture et une ouverture sur la même image

Lorsque l'on enchaine une ouverture et une fermeture, on obtient cette image:<br>
<img src="TP2/img/fermouv1.png" alt="fermouv1.png" width="200"/>

On peut voir que le résultat est assez grossier, c'est pourquoi nous allon maintenant tenter de faire 3 dilatations, puis 6 érosions et enfin 3 dilatations afin de voir si l'image est déjà moins brute.

#### 4. Encore et encore

On obtient ces différentes images avec les 3 dilatations, suivies de 6 érosions et enfin 3 dilatations:<br>

>./dilatation slenderman4.pgm test1.pgm
<img src="TP2/img/test1.png" alt="test1.png" width="200"/>

>./dilatation test1.pgm test2.pgm
<img src="TP2/img/test2.png" alt="test2.png" width="200"/>

>./dilatation test2.pgm test3.pgm
<img src="TP2/img/test3.png" alt="test3.png" width="200"/>

>./erosion test3.pgm test4.pgm
<img src="TP2/img/test4.png" alt="test4.png" width="200"/>

>./erosion test4.pgm test5.pgm
<img src="TP2/img/test5.png" alt="test5.png" width="200"/>

>./erosion test5.pgm test6.pgm
<img src="TP2/img/test6.png" alt="test6.png" width="200"/>

>./erosion test6.pgm test7.pgm
<img src="TP2/img/test7.png" alt="test7.png" width="200"/>

>./erosion test7.pgm test8.pgm
<img src="TP2/img/test8.png" alt="test8.png" width="200"/>

>./erosion test8.pgm test9.pgm
<img src="TP2/img/test9.png" alt="test9.png" width="200"/>

>./dilatation test9.pgm test10.pgm
<img src="TP2/img/test10.png" alt="test10.png" width="200"/>

>./dilatation test10.pgm test11.pgm
<img src="TP2/img/test11.png" alt="test11.png" width="200"/>

>./dilatation test11.pgm test12.pgm
<img src="TP2/img/test12.png" alt="test12.png" width="200"/>


## 4) Segmentation d’une image



#### 1. Création du fichier difference.cpp

Pour le code [difference.cpp](TP2/difference.cpp), j'ai prit une certaine liberté que j'explique en dessous:<br> 
-On ne prend pas différentes hauteurs et largeurs, car on sait ici que ce sont les mêmes, car on prend deux images qui ont une résolution de 256x256. Sinon, on aurait dût faire une variable pour chaque mesure.

On obtient cette image en finalité:
>./difference slenderman4.pgm test12.pgm res1.pgm
<img src="TP2/img/res1.png" alt="res1.png" width="200"/>


## En conclusion

Ce que je peux dire sur ce TP, c'est que difference.cpp permet d'obtenir une image plutôt ressemblante à l'originale contrairement à la dilatation, l'érosion, l'ouverture ou la fermeture.<br>
Les difficultés pour cet TP a été la mauvaise compréhension du code. Je pensais pendant toute la séance que par exemple pour la dilatation, si pour un pixel blanc, au moins un pixel à côté est blanc, on le garde blanc sur l'image de retour et inversement pour 
l'érosion.

Sinon, je n'ai pas trouvé ce TP dur.<br>
Seulement, j'aurais bien aimé faire les bonus des deux premiers TP. Peut-être que je les ferais plus tard, mais en ce moment je passe par un moment plutôt difficile à encaisser niveau personnel, alors je n'ai pas forcément la tête à ça. Désolé.
